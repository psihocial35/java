package com.company;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class MainTest 
{

    @Test
    public void testSum() throws Exception
    {
        assertEquals(10,Main.sum(7,3),"SUM GOOD");
        System.out.println("Sum OK");
    }
    
    @Test
    public void testMin() throws Exception
    {
        assertEquals(6,Main.min(9,3), "MIN GOOD");
        System.out.println("Min OK");
    }
    
    @Test
    public void testUmn() throws Exception
    {
        assertEquals(6,Main.umn(2,3), "UMN GOOD");
        System.out.println("Umn OK");
    }
    
    @Test
    public void testDel() throws Exception
    {
        assertEquals(4,Main.del(20,5), "DEL GOOD");
        System.out.println("Del OK");
    }
}